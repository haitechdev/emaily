import React from 'react';

export default ({ input, label, meta: { error, touched } }) => {
    return (
        <div style={{marginBottom: '15px'}} >
            <label>{label}</label>
            <input style={{marginBottom: '5px'}} {...input} />
            <small className="red-text">{touched && error}</small>
        </div>
    )
}